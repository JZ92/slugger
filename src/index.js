const slug = (...strings) => {
  const regex = /\s+/g;

  return strings
    .map((string) => string.toLowerCase().trim().split(regex).join("-"))
    .join("-");
};

module.exports = { slug };
