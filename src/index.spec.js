const { slug } = require("./index.js");

/**
 * @describe [optional] - group of tests with a header to describe them
 */
describe("testing slugger basic functionality", () => {
  /**
   * @it - unit tests can use the 'it' syntax
   */

  it("slugger can slug string with spaces", () => {
    expect(slug("hi     sir    that is a test    ")).toEqual(
      "hi-sir-that-is-a-test"
    );
  });
  /**
   * @test - unit test can use the 'test' syntax
   */

  it("slugger can slug any number of spacy strings", () => {
    expect(slug("hi there   ", "how are  you  ", "today?")).toEqual(
      "hi-there-how-are-you-today?"
    );
  });
});
