# Slugger

Slugger is a JavaScript library that accepts any number of strings as arguments and returns a single string with hyphens between each word

```bash
npm i @jzach/slugger
```

## Usage

```JavaScript
const {slug} = require("@jzach/slugger");

slug.pluralize("Hi", "this is     me", "I hope    you will have an     AWESOME day"]) # returns "hi-this-is-me-i-hope-you-will-have-an-awesome-day"

```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
